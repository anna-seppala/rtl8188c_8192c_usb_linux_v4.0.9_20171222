# README #

This is a minimally modified driver for the edimax EW-7612UAn v2 WiFi adapter

### What is this repository for? ###

* This repo provides a version of the WiFi adapter driver that works with Raspberry Pi (tested on Pi 3 Model B v1.2)
* This version is in no way stable to use, it works just because I commented out stuff that was giving compilation errors.

### How do I get set up? ###

* run Makefile with
	make clean
	make
	
* manually install by copying the resulting 8192cu.ko file to /lib/modules/<kernel-version>/kernel/net/wireless/
* then, run
	sudo modprobe -a 8192cu
* to uninsert the module, run "sudo modprobe -r 8192cu"

* now you should see a wireless interface if you run
	iwconfig
